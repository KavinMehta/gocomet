@if ($freights['data']['shipment'] == null)
    <div class="h1">No Frieghts Available</div>
@else
    <div class="h2 mt-3"> These rates are valid from {{ Carbon\Carbon::now()->startOfWeek()->format('Y-m-d') }} to
        {{ Carbon\Carbon::now()->endOfWeek()->format('Y-m-d') }}</div>
    @foreach ($freights['data']['shipment'] as $shipment)
        @if ($shipment['freight'][0]['shippingLine'] != null)
            <div class="card mt-3">
                <div class="row ms-3">
                    <div class="col-md-8 margin-right">
                        <div class="row align-items-center mt-3">
                            <div class="col-md-2 img-logo">
                                <img src="{{ $shipment['freight'][0]['logo'] }}" alt="" class="img-fluid">
                            </div>
                            <div class="col-md-10">
                                <div class="card-title h2">{{ $shipment['freight'][0]['shippingLine'] }}</div>
                            </div>
                        </div>
                        <div class="row h3">
                            <div class="col-3">
                                {{ $shipment['portFrom']['name'] }}
                            </div>
                            <div class="col-6 text-center">
                                {{ $shipment['freight'][0]['transitTime'] }}
                            </div>
                            <div class="col-3 ">
                                <span class="float-end">
                                    {{ $shipment['portTo']['name'] }}
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 bg-primary text-white">
                        <div class="row h4 mt-3">
                            <div class="col-6">Price:</div>
                            <div class="col-6">$ {{ $shipment['freight'][0]['price'] }}</div>
                            <div class="col-6">Distance</div>
                            <div class="col-6">{{ $shipment['freight'][0]['distance'] }}</div>
                            <div class="col-6">ID:</div>
                            <div class="col-6">{{ $shipment['shipmentId'] }}</div>
                            <div class="col-6">CO2</div>
                            <div class="col-6">{{ $shipment['freight'][0]['co2'] }}g</div>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    @endforeach
@endif
