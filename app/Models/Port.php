<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Port extends Model
{
    use HasFactory;

    public static function get_port_name($search)
    {
        $ports = Port::where('port_name', 'LIKE', '%' . $search . '%')->get();
        return $ports;
    }

    public static function get_coordinates($port_code)
    {
        $port = Port::where('port_code', $port_code)->first();
        return $port;
    }
}
