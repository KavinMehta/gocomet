<?php

namespace App\Http\Controllers;

use App\Models\Port;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class HomeController extends Controller
{
    public function index()
    {
        return view('welcome');
    }

    public function get_freight(Request $request)
    {
        // dd($request);
        $start_port = Port::get_coordinates($request->start_port_id);
        $end_port = Port::get_coordinates($request->end_port_id);
        $apiURL = 'https://www.searates.com/graphql_rates';
        $postInput = [
            'query' => "\n{\n  shipment: fcl(from: [" . $start_port->port_lat . ", " . $start_port->port_long . "], to: [" . $end_port->port_lat . ", " . $end_port->port_long . "], ST20: 1, currency: USD, date: \"2022-06-18\", source: \"le\", ) {\n    shipmentId\n    transportationMode\n    incoterms\n    currency\n    cityFrom: city(mode: EXPORT) {\n      ...cityFields\n    }\n    cityTo: city(mode: IMPORT) {\n      ...cityFields\n    }\n    portFrom: port(mode: EXPORT) {\n      ...portFields\n    }\n    portTo: port(mode: IMPORT) {\n      ...portFields\n    }\n    freight: oceanFreight {\n      ...ratesFields\n    }\n  }\n  default {\n    services\n    bookingViaEmail\n  }\n  identity {\n    first_name\n    last_name\n    admin\n    carrier\n    dfa\n    email\n    phone\n  }\n}\n\nfragment ratesFields on OceanFreight {\n  shippingLine\n  logo\n  containerCode\n  validTo\n  containerType\n  quantity\n  linerTerms\n  originalPrice\n  originalCurrency\n  price\n  distance\n  transitTime\n  transportFrom\n  transportTo\n  alternative\n  overdue\n  co2\n  comment\n  rateOwner\n  portFeesFrom: portFees(mode: EXPORT) {\n    ...portFeesFields\n  }\n  portFeesTo: portFees(mode: IMPORT) {\n    ...portFeesFields\n  }\n  truckFrom: truck(mode: EXPORT) {\n    originalPrice\n    originalCurrency\n    price\n    distance\n    transitTime\n    interpolation\n    co2\n    comment\n    rateOwner\n  }\n  truckTo: truck(mode: IMPORT) {\n    originalPrice\n    originalCurrency\n    price\n    distance\n    transitTime\n    interpolation\n    co2\n    comment\n    rateOwner\n  }\n  railFrom: rail(mode: EXPORT) {\n    originalPrice\n    originalCurrency\n    price\n    distance\n    transitTime\n    interpolation\n    co2\n    comment\n    rateOwner\n  }\n  railTo: rail(mode: IMPORT) {\n    originalPrice\n    originalCurrency\n    price\n    distance\n    transitTime\n    interpolation\n    co2\n    comment\n    rateOwner\n  }\n  dryFrom: dry(mode: EXPORT) {\n    price\n    distance\n    transitTime\n    interpolation\n    city(mode: EXPORT) {\n      ...cityFields\n    }\n  }\n  dryTo: dry(mode: IMPORT) {\n    price\n    distance\n    transitTime\n    interpolation\n    city(mode: IMPORT) {\n      ...cityFields\n    }\n  }\n    bargeFrom: barge(mode: EXPORT) {\n    price\n    distance\n    transitTime\n    validTo\n    currency\n    co2\n    port: portFrom {\n      ...portFields\n    }\n  }\n  bargeTo: barge(mode: IMPORT) {\n    price\n    distance\n    transitTime\n    validTo\n    currency\n    co2\n    port: portTo {\n      ...portFields\n    }\n  }\n}\n\nfragment cityFields on City {\n  id\n  name\n  code\n  countryCode\n  lat\n  lng\n}\n\nfragment portFields on Port {\n  id\n  name\n  code\n  countryCode\n  lat\n  lng\n  inaccessible\n}\n\nfragment portFeesFields on PortFees {\n  abbr\n  title\n  text\n  originalPrice\n  originalCurrency\n  price\n  perLot\n  co2\n}\n"
        ];
        $headers = [
            'Authorization' => 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJwcm9maWxlIjpudWxsLCJwbGF0Zm9ybSI6MSwibmFtZSI6IiIsImlzcyI6InNlYXJhdGVzLmNvbSIsImlhdCI6MTY1NTQ2NzE2MiwiZXhwIjoxNjU1ODk5MTYyLCJ0b2tlbiI6bnVsbH0.T6ZAS9suUB81ac83YC24IsNB6ua1SLpdWPHNLJ1-nqg'
            // 'X-NewRelic-ID' => 'VwIBUFNbARADXFlRAQMBX1M='
        ];

        $response = Http::withHeaders($headers)->post($apiURL, $postInput);

        $statusCode = $response->status();
        $responseBody = json_decode($response->getBody(), true);
        $returnHTML = view('ajax-freight', ['freights' => $responseBody])->render();
        return response()->json($returnHTML);
        // dd($responseBody);
    }
}
