<?php

namespace App\Http\Controllers;

use App\Models\Port;
use Illuminate\Http\Request;

class PortsController extends Controller
{
    public function get_port(Request $request)
    {
        // dd($request->search);
        $ports = Port::get_port_name($request->search);
        // dd($ports);
        $data = [];
        foreach ($ports as $port) {
            $local = [];
            $local['id'] = $port->port_code;
            $local['text'] = $port->port_name;

            $data[] = $local;
        }
        // dd($data);
        return $data;
    }
}
