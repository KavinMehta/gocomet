<?php

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/', [\App\Http\Controllers\HomeController::class, 'index'])->name('welcome');

Route::get('/test', [\App\Http\Controllers\Test::class, 'index'])->name('test');

// API

Route::get('/get_port', [\App\Http\Controllers\PortsController::class, 'get_port'])->name('get_port');
Route::post('/get_freight', [\App\Http\Controllers\HomeController::class, 'get_freight'])->name('get_freight');
